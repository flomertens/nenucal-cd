# Change Log

## 0.7.1 - 2024-07-22

- [New] calipipe: Add --n_cpu to soltool plot call
- [New] add option --hosts to nenudata l1_to_l2
- [New] modeltool attenuate: predict beam over the full bandwidth range. Add beamsquint_frequency_mhz parameter
- [Changed] calpipe: remove quality update at the end of the flagger task
- [Fixed] calpipe: make sure solutions_per_direction string is compatible with newer DP3

## 0.7 - 2024-07-10

- [New] nenudata l1_to_l2 add timeslot_multiple parameter to make sure nbs of time slots in time-split MS are multiple of timeslot_multiple
- [New] datahandler: add the possibility to create composite spectral windows which is a combination of other spectra windows (like SW00 = ['SW01', 'SW02'])
- [Changed] DPPP -> DP3

## 0.6.2 - 2024-07-09

- [Changed] update requirements

## 0.6.1 - 2024-07-09

- [Changed] Update astropy requirement


## 0.6 - 2024-07-09

- [Changed] Deprecate usage of sshpass
- [New] nenudata: Add option to retrieve only specific SWs
- [Changed] vis_flagger: add a hpass_filter option to the flag_freqs_band
- [Changed] vis_flagger: add flag_time_freq_outliers
- [Changed] vis_flagger: make flagger steps optional, flag_time_freq_outliers is not on by default
- [Changed] datahandler: allow to set multiple identical nodes in the list of 2 nodes

## 0.5 - 2023-04-28

- [New] Add solveralgorithm and solutions_per_direction parameter to ddecal task
- [New] Add numthreads worker parameter
- [New] Add new flagttol vis_flagger command

## 0.4 - 2023-02-12

- [New] nenucal subtract step: copy column when no patch to subtract
- [New] Support %YEAR%, %MONTH%, %OBS_ID% in all L1 data path
- [New] datahandler: If obs_id is already in the data_path, do not add it at the end
- [New] Add --combine_obs_ids option to imgpipe. By default obs_ids are not combine.

## 0.3 - 2022-12-19

- [New] Update skymodel.py to nenupy >= 2.0. Updated dependency to nenupy >=2.0
- [New] nenudata: add --env_file option to l1_to_l2 command
- [New] nanudata: add --run_on_host option to retrieve command
- [New] datahandler: add n2_nodes to get_all_hosts()
- [New] nenudata: add an option to append a postfix to the outputed MS paths
- [Fixed] skymodel: polarization parameter of the compute_beam functions was not correctly set.
- [Fixed] skymodel: fix skymodel.concatenate

## 0.2.1 - 2022-10-19

- [Fixed] In get_ms_freqs(), use reshape instead of squeeze()

## 0.2 - 2022-07-24

- [New] Implement N2 data level with MS split in time slices over the different nodes
- [Fixed] Improve solution smoothing.
- [Fixed] Fix flagtool loading wrong msutils module.
- [Fixed] Fix modeltool loading wrong skymodel module.
- [Fixed] soltool: improve opening solution file without directions.
- [Fixed] Fix frequency range flagging.

## 0.2 (RC1) - 2022-01-13

- [Fixed] skymodel.edit_model: If min_elevation_patch <= 0, do not check patch for elevation
- [Fixed] fix aoquality command line

## 0.2 (beta) - 2021-11-30

- [New] add imgpipe command line tool.
- [New] add the option skymodel.int_ateam_sky_model to set a different Ateam sky model than the default one.
- [New] add the possibility to subtract without applying a calibration table by specifying an empty cal.parmdb.
- [New] add predict task.
- [New] add the option skymodel.app_sky_model_file to set a specific main field apparent sky model.
- [Changed] improve "modeltool build" messages.
- [Changed] update Ateam sky model

## 0.1.11  - 2021-07-13

- [Changed] update dependencies to allow python >=3.6, astropy >=3.2, keyring>=20, and include losoto from pypi. LSMtool still not include you have to install it with pip afterwards.

## 0.1.10  - 2021-07-01

- [Fixed] SkyModel.get_directions() was not always returning the correct patch

## 0.1.9  - 2021-05-18

- [Changed] soltool smooth: use astropy convolve function to smooth the data, it should be more reliable for missing data.

## 0.1.8  - 2021-04-30

- [Fixed] update dependencies

## 0.1.7  - 2021-04-30

- [Added] nenucal.msutils has been moved from libpipe.msutils.
